**General**

- [[Home]]
- [[Features]]
- [[Troubleshooting]]

**[[How to use]]**

- [[Recording your first workout]]
    - [[Heartrate]]
    - [[Custom Workout Types]]
- [[Manually add workouts]]
- [[Workout Info]]
    - [[Upload to OSM]]
    - [[Cutting Workouts]]
- [[Settings]]
    - [[Auto Export]]
    - [[Offline Maps]]
- [[Workout Statistics]]

**More**

- [[Contributing]]
    - [[Translation]]
- [[Permissions]]