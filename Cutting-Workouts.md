## Introduction

To have precise statistics of your activity it is important to accurately track the moment the activity starts and stops. This is normally done by starting and stopping the recording just before and after the activity.

This is not always convenient as the phone typically needs to be stored somewhere and unlocked after the activity. FitoTrack has a built-in feature that allows you to start and stop the recording a few minutes before/after the activity starts while keeping your statistics accurate. This feature is called "Edit Start/End" and it allows you to pinpoint the start and end of the workout after the activity has been performed.

## Recording

When planning to use the Cut feature the timining is more relaxed. Start the tracking a few minutes as before the training starts, perform your activity and stop tracking once done. There is no time pressure.

## Cutting

Once you have cooled down go to the activity screen and press "Edit start/End". You are presented with a screen showing the track on the map and a graph bellow. Select a position on the map or the diagram. It can help to zoom in. 

A typical flow is to look at the time/speed graph and zoom to where the speed matches the start of the training. Once clicked, the maps also shows the selected point. It can help to zoom in further and click to move the point around. If you are satisfied with the starting position click on the "<" left arrow on top to select the starting point. This will mark the area that will be removed at the start in gray. Note that it still is possible to select an other point in the map and click the "<" arrow again.
Do the same for the end point. When you're done, click on the save icon to apply the changes. 

## Recovery

**The changes that are made are permanent** and it is not possible to undo a cutting action once saved. You can export the workout to a gpx file first to be able to recover if this is needed.


