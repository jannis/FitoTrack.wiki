You can add a workout manually by entering the data without recording it. In the workout list click on the plus icon and enter workout. You get into a screen where you can enter the data:

* Type
* Distance
* Date
* Start Time
* Duration
* Comment

Press the plus icon in the top right corner to add the workout.