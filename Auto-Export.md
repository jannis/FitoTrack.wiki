# Auto Export

FitoTrack supports exporting GPX files and app backups. You can access relevant settings under: Settings > Database

## Set up auto export

You can set up exports for:

- workouts: A GPX file will be exported after a workout was recorded.
- app backups: Complete app backups. You can set the interval under Settings > Database > Backup interval

## Targets

### Directory

The files will be written into the specified directory.

### HTTP POST request

The file will be uploaded with a HTTP POST request including the file as content. Additionally there are some HTTP headers sent with the request:

| Key                    | Description                                     | Example                   |
| ---------------------- | ----------------------------------------------- | ------------------------- |
| FitoTrack-Type         | Type of exported file                           | `backup` or `workout-gpx` |
| FitoTrack-Timestamp    | Timestamp of data in millis                     | `1652365132702`           |
| FitoTrack-Workout-Type | Type of workout                                 | `running`/`cycling`/...   |
| FitoTrack-Comment      | Safe comment for workout (ASCII, max 50 chars)  | `around the lake`         |

### Nextcloud

Direct exports to Nextcloud are not supported yet but you can still use it. Just create a directory where FitoTrack puts new GPX files and set up auto upload in the nextcloud app under Nextcloud > Settings > Auto upload > Menu in the top right corner > Set up a custom folder, then choose local and remote folder.