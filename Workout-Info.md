# Workout Info

To see more information about the workout click on it in the list. You enter a screen where your workout is summarized.

<img src="https://codeberg.org/jannis/FitoTrack/media/branch/master/doc/screenshots/screenshot3.png" width="300"/>

* Comment: the comment you chose. Click on it to edit.
* Date: date of workout
* Duration: the duration exclusive breaks
* Pause duration: the duration of all breaks you took
* Start/End Time
* Distance
* Pace
* Route: click on the map to view a fullscreen map
* Speed (speed over time)
* Heart rate over time
* Burned energy: this is an assumption of the calories which were burned
* Height (height over time)

## Charts

You can click on on one of the diagrams. Then you can swipe over the diagram to see the corresponding locations. You can also display two different things together. (E.g. to see how your speed changed when going up the hill)

## Menu

Click on the three points in the upper right corner to open the menu.

* Resume Workout: You can resume the last saved workout. Keep in mind that this will create a long pause between the point you stopped the workout and the current time.
* Edit Workout: Edit workout data. It will show the same screen as when you [[Manually add workouts]].
* Edit Start/End: see [[Cutting Workouts]]
* [[Upload to OSM]]
* Export as GPX-File: You can export the workout as a GPX-file and share it with other apps and services or save it to a folder.
* Delete
* Share: Share the workout as an image
