# Upload to OSM

If you want to upload your workout as GPS-Track to [OpenStreetMap](https://www.openstreetmap.org/) follow these steps:

## Authenticate

This only has to be done once. An authorisation key will be saved for future access.

1. Open a workout with GPS location data
2. Click on the option "Upload to OSM" in the menu, you will be redirected to the OpenStreetMap website
3. Login if you haven't already
4. Permit FitoTrack access to upload GPS-Tracks to your account
5. Copy the displayed authentification token
6. Go back to FitoTrack, you should see a dialog
7. Enter the copied code into the text field and click okay.

## Upload

Enter the data into the dialog:

* Description: will be put into the uploaded track
* Track Visibility: please see the documentation in the [OSM wiki](https://wiki.openstreetmap.org/wiki/Visibility_of_GPS_traces)
* Cutting: if checked the app will cut the first and last 300 meters to prevent the traces revealing private places such as your home

Then hit upload. The trace will be uploaded and usually you get a mail from OpenStreetMap as confirmation.