You can translate FitoTrack on **[Weblate](https://hosted.weblate.org/engage/fitotrack/)**.

## Translation Progress

<a href="https://hosted.weblate.org/engage/fitotrack/">
<img src="https://hosted.weblate.org/widgets/fitotrack/-/android/multi-auto.svg" alt="Translation state" />
</a>