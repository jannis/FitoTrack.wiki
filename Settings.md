# Settings
The settings are split into three categories.

* User Interface
* Recording
* Database

## User Interface

### System of units

FitoTrack supports the following unit systems:

* Metric
* Metric with m/s
* Imperial
* Imperial with meters
* US Customary

### Your weight

The weight is needed to calculate the calories you burned.

### Step length

Needed to estimate distance for workouts without GPS.

### Map Style

* Mapnik: default, reliable
* Humanitarian: clean, but I noticed it's sometimes not available

### Track coloring & Track style

FitoTrack can color the route on the map according to different values such as speed or height. You can also choose a style to color those maps.

### Offline map

see [[Offline Maps]]

### Theme

You can change the app theme between *light* and *dark*. To apply the changes the app has to be restarted.

### Date Format

Different formats to display dates.

### Time Format

Different formats to display times.

### Energy Unit

* kcal
* Joule

## Recording

### Voice announcements

If you like, the app can inform you about current data about your workout. 
To use that you need to have a TextToSpeech engine installed.

#### Announcement trigger

Configure when the announcement should be triggered.
Choose the time and distcance between the voice announcements.

#### Announcement Mode

* Always: Announce always
* Only on headphones: Announce only if headphones are connected

#### Content

* GPS Status: played when the phone looses or finds the GPS signal
* Duration
* Distance
* Current Speed
* Average Speed (in motion): excluding pauses
* Average Speed (Total): including pauses
* Average Speed (last minute)
* Ascent
* Current Time of the day
* Average Pace

### Interval Training

You can create interval sets which can be used during the workout. ([[How to Use]])
To use that you need to have a TextToSpeech engine installed.

#### Create Interval set

Click on the plus icon and enter the name of the set. You will be redirected into a screen where you can edit the set.

#### Edit a set

In the edit screen click on the plus icon to add intervals. Their names will be played. To delete an interval, click on the trash bin and if you want to delete the whole set use the trash bin icon in the upper right corner.

#### Use a set

please see [[Recording your first workout]]

### Auto Workout Timeout

A recording workout will be stopped and saved automatically when there was no movement for the timeout.

### Include pause time in Interval Training

If this is enabled, the intervals include pauses. That means if you take a break the interval training continues.

## Database

### Import / Export

- Imports a backup from the filesystem.
- Exports a backup and shares it with other apps and services or saves it to a folder on the filesystem.

### Automatic backups

see [[Auto Export]]

## About

A screen containing general infos as well as the contributors and translators of FitoTrack.