If you open the app you see the workout overview. To record a new workout click on the plus icon in the bottom right corner then click record workout and select the workout type you are about to perform.

<img src="https://codeberg.org/jannis/FitoTrack/raw/branch/master/doc/screenshots/screenshot6.jpg" width="300"/>

Now the app waits until the device finds its location via GPS. If the GPS signal is strong enough to record, the app will show a Start-Button. Click that button to start the recording.

<img src="https://codeberg.org/jannis/FitoTrack/media/branch/master/doc/screenshots/screenshot2.png" width="300"/>

During the workout the screen provides you with information like time, distance, average speed, etc. and a map with the current track. When you're done just hit stop. Optionally you can enter a comment. (can also be edited later)

## I have a problem

See [[Troubleshooting]]. If you don't find a solution you can create an [issue](https://codeberg.org/jannis/FitoTrack/issues) in this repository.

## Customize the displayed information

If the recording hasn't started yet, you are able to click on the information fields to select what should be displayed.

You can display the following information:

* Distance
* Pause duration
* Average Speed (in Motion): excluding pauses
* Average Speed (Total): including pauses
* Current Speed
* Ascent
* Burned Energy
* Average Pace

## Use your heart rate sensor

see [[Heartrate]]

## Use interval sets / Interval training

To use that you need to have a TextToSpeech engine installed.

If you want to use a interval set which you have previously configured (*see [[Settings]]*) click the list icon next to the stop button. Then you can select the interval set you would like to use.

If the icon doesn't appear, please check that:

* you have a TextToSpeech engine installed
* and the workout hasn't started yet