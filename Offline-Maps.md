# Offline Maps

## Use offline maps

The relevant settings can be found under Settings > User Interface > Map

1. Select "offline" as your map style
2. Specify a folder where you have stored map files or where FitoTrack can save them

## Download maps

If you haven't downloaded maps yet, you can do so by clicking on "Download Maps". Then navigate to your preffered region and confirm the download.

You can also manually download offline map files from here: http://ftp-stud.hs-esslingen.de/pub/Mirrors/download.mapsforge.org/maps/v5/

## Render Styles

To be written...