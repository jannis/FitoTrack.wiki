# How to use FitoTrack

* [[Recording your first workout]]
    * [[Heartrate]]
    * [[Custom Workout Types]]
* [[Workout Info]]
    * [[Upload to OSM]]
    * [[Cutting Workouts]]
* [[Manually add workouts]]
* [[Workout Statistics]]
* [[Settings]]
    * [[Auto Export]]
    * [[Offline Maps]]