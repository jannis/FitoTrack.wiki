This describes why the app needs the permissions it requests:

* `ACCESS_FINE_LOCATION` and `ACCESS_COARSE_LOCATION`: GPS-Tracking
* `INTERNET`: Fetching the map tiles / Upload GPS-traces to OpenStreetMap
* `BLUETOOTH`: Voice Announcements, check if headphones are connected (until Android 11)
* `BLUETOOTH_ADMIN`: Use heart rate sensors (until Android 11)
* `BLUETOOTH_SCAN`: Scan for heart rate sensors
* `BLUETOOTH_CONNECT`: Connect to heart rate sensors
* `WAKE_LOCK`: Stay awake while recording
* `FOREGROUND_SERVICE`: Start the recording service
* `FOREGROUND_SERVICE_LOCATION`: Start the gps recording service
* `FOREGROUND_SERVICE_HEALTH`: Start the indoor recording service
* `READ_EXTERNAL_STORAGE`: Import backups and GPX files, read offline maps
* `WRITE_EXTERNAL_STORAGE`: Export backups and GPX files, download offline maps
* `NFC`: start activities when scanning NFC tags
* `VIBRATE`: vibrate when auto-start starts an activity
* `ACTIVITY_RECOGNITION`: Recording step counts
* `POST_NOTIFICATIONS`: Show current workout progress