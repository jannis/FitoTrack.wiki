# Heart rate measurement

FitoTrack allows heart rate monitoring using the Bluetooth Heart Rate Profile. This is a vendor
independent protocol supported by many commercially available heart rate sensors. Pair your
device in Android's Bluetooth settings first, then it will become available in FitoTrack.

To record heart rate during workout, the Bluetooth device needs to be connected before the
workout starts. Click on the three dots in the upper right corner and select "Connect heart
rate device". FitoTrack will provide a list with all available devices. Please select your sensor. Afterwards, FitoTrack will try to connect to the device.

<img src="screenshot_heartrate.png" width="300"/>

To display live heart rate during recording, click on one of the four display fields and set it to
heart rate display. This also needs to be done before recording because the display fields can't
be changed while the recording is running.

A small green Bluetooth icon will appear in the lower left corner of the map when your device is
connected. This does not mean that it actually sends data though, so make sure to check if
a heart rate is actually displayed.

When starting a new workout, FitoTrack will try to connect to the last connected Bluetooth device.

Many activity trackers only measure heart rate if you start an activity first. Also, for some
devices the vendor independent Heart Rate Profile needs to be enabled in it's app (or with
[Gadgetbridge](https://codeberg.org/Freeyourgadget/Gadgetbridge/wiki/Huami-Heartrate-measurement)).

The following devices are reported to work in FitoTrack:

|Device              |Supported|Remarks                                                               |
|--------------------|---------|----------------------------------------------------------------------|
|PineTime (+[InfiniTime](https://github.com/JF002/InfiniTime))|Yes    | |
|Polar H9            | Yes     |                                                                      |
|Polar H10           | Yes     |                                                                      |
|Xiaomi Mi Band 3 / 4|Partially|Enable "3rd party realtime HR access" with Gadgetbridge and start activity (Treadmill or Exercise) on the tracker. Connection is sometimes lost, so check that heart rate is actually displayed.|
| Coospo HW807       | Yes ||
| Lenovo HX03W       | No? ||
|                    |         |                                                                      |
