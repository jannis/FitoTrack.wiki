Welcome to the Wiki of FitoTrack.

## Table of contents
* [[How to use]]
* [[Features]]
* [[Heartrate]]
* [[Troubleshooting]]
* [[Permissions]]
* [[Contributing]]