## Workout Statistics

TODO: rewrite for update statistics update (version 15)

To enter the workout statistics, click the three-point menu in the upper-right corner in the workout list.

- Select Type filter: First select the workout type you want to see statistics about by clicking the type.
- Select Information: Then select the information you want to see like Distance, Duration, etc.
- Select Time Span: Now select the time span, in which the workouts should be merged. That means if you select month, all workouts of one month will be put together.

### Example

It can be complicated to understand so I'll describe it with an example here: We filter by cycling workouts, and want to see the distance of each month. Now all the distances of each month are summed up.