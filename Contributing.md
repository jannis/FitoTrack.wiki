To contribute to the project you need to create an account on Codeberg which is a free and open source platform. If you're not sure whether your change/function is wanted, please create an issue first.

## Be listed as a contributor

Feel free to add yourself to the contributor list which is located at `./app/src/main/res/values/contributors.xml` ([Link](https://codeberg.org/jannis/FitoTrack/src/branch/master/app/src/main/res/values/contributors.xml))

## Translation

*see [[Translation]]*