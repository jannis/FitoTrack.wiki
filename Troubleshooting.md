# Troubleshooting

## Recording shows "Waiting for GPS"

Please ensure that:

- FitoTrack has location permission (go to System Settings > Apps > FitoTrack > Permissions > Location).
- your device receives a stable GPS signal. You can check that with apps like [GPS Test](https://f-droid.org/en/packages/com.android.gpstest.osmdroid/).
    - for a better GPS signal go outside
    - wait a minute

## Recording shows a map but the button says "Cannot Start"

Look at the GPS text in the bottom right corner of the map:

- it's Red: look at Question "Recording shows Waiting for GPS"
- it's Yellow: the GPS signal is too bad for FitoTrack to track your location. Try to go to a location with clear sky. Mostly you just have to wait some seconds.
- it's Green: Hit the start button, it works ;)

## Recording stops / looses parts or the entire workout

Ensure that:

- no battery saver is enabled
- your device has a reasonable stable GPS
    - In the most cases this is not your problem but sometimes it can occour. This might result in shifted paths and parts of the workout being displayed as direct lines.

What you can try:

- disable "battery optimizations"
    - Go to system settings > Apps > Advanced > Special app access > Battery optimization > All apps > FitoTrack > Don't optimize
    - this is complicated and will be made more easy in a future release of FitoTrack


## I cannot use voice announcements or interval sets

Please ensure that you have a TextToSpeech-Engine installed. In most cases you need to install Google Text-To-Speech.