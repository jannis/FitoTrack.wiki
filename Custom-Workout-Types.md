FitoTrack allows you to add your own workout types. Go to Settings > Custom Workout Types. Now click on the plus icon in the bottom right corner to add a new workout type.

Enter the following information:

- **ID:** a unique identifier for your type. It is also used in GPX-exports. This cannot be changed later.
- **Name:** the displayed name
- **Sample distance:** The minimum distance between two GPS data points. Set higher for faster activities and smaller for slower. Walking could be 5m and cycling 10m.
- **MET:** ([Metabolic equivalent of task](https://en.wikipedia.org/wiki/Metabolic_equivalent_of_task)) This is a measure for how much energy is burned while doing an activity. You can find good values [here](https://sites.google.com/site/compendiumofphysicalactivities/Activity-Categories).
- **Icon:** select an icon to be displayed